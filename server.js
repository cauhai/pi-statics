const express = require('express');
const app = express();
const hostname = '0.0.0.0';
const port = 8080;

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/cdn', express.static('public'))

app.get('/', (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.end('<h3>A bunch of static files here; you probably do not care :)</h3>');
});

app.listen(port, hostname, () => {
  console.log(`Static file server, running at http://${hostname}:${port}/`);
});

