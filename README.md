# pi-statics

Node server, serving static files, typically JS files for plugins.

To start, run `node server.js`
then check something like 
`http://localhost:8080/cdn/assets/images/obelix.jpg`
